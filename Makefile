
TAP_TESTS = 1

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

# releases before 12 didn't include prove_installcheck in the installcheck
# target, so we need to do it here for those.
NEEDTARGET = $(shell test $(VERSION_NUM) -lt 120000 && echo yes)
ifeq ($(NEEDTARGET),yes)
installcheck:
	$(prove_installcheck)
endif
