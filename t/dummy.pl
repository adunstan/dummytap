#!/usr/bin/perl

use strict;
use warnings;

use PostgresNode;
use Test::More;

my $node = PostgresNode->get_new_node('dummy');

$node->dump_info;

$node->init;

$node->start;

$node->stop;

ok(1,"dummy");
done_testing();
